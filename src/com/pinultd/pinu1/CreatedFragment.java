package com.pinultd.pinu1;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class CreatedFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.created_fragment_layout, container, false);
		
		Button myBtn = (Button) v.findViewById(R.id.myBtn);
		final TextView txtMsg = (TextView) v.findViewById(R.id.txtMsg);
		
		myBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				txtMsg.setText("This is the new fragment.\n Thank you for this opportunity.");
			}
		});

		return v;
	}
}
