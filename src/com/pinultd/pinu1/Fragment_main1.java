package com.pinultd.pinu1;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Fragment_main1 extends Fragment {
	public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_main1, container, false);
		
		// to ensure that the app won't crash in case there's no message passed
		if(getArguments() != null) { 
			String message = getArguments().getString(EXTRA_MESSAGE);
			/*
			 * The TextView element with the id 'textView' does not exist
			 * So, I removed the R.id.textView and replaced it with 
			 * R.id.section_label
			 */
			TextView messageTextView = (TextView) v.findViewById(R.id.section_label);
			messageTextView.setText(message);
		}
		return v;
	}
	
}
