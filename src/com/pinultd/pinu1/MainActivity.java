package com.pinultd.pinu1;

import java.util.List;
import java.util.Vector;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends FragmentActivity {
	/* It will still work fine when extending Activity,
	 * But it's more safe to extend FragmentActivity [Well, yes! since we're working with Fragments]
	 */
	
	public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";

	ViewPager mViewPager;
	MyPageAdapter myPageAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		/*
		 * Initialize a fragments list with Fragments to show
		 * Also, the myPageAdapter is initialized inside of initializePaging
		 */
		initializePaging();
		
		mViewPager = (ViewPager) findViewById(R.id.viewpager);
		mViewPager.setAdapter(myPageAdapter);
	}

	private void initializePaging() {
		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, Fragment_main.class.getName()));
		fragments.add(Fragment.instantiate(this, Fragment_main1.class.getName()));
		fragments.add(Fragment.instantiate(this, Fragment_main2.class.getName()));

		// Added the My_fragment to the list of fragments to show it
		fragments.add(Fragment.instantiate(this, My_fragment.class.getName()));
		
		/*
		 * Secondly create your own fragment and then include it into the swipe sequence
		 * So, I created a new fragment called CreatedFragment
		 * and added it to the list
		 */
		fragments.add(Fragment.instantiate(this, CreatedFragment.class.getName()));

		myPageAdapter = new MyPageAdapter(getFragmentManager(), fragments);
		ViewPager pager = (ViewPager) findViewById(R.id.viewpager);
		
		// Set myPageAdapter as the pager adapter (instead of mSectionsPagerAdapter)
		pager.setAdapter(myPageAdapter);
	}

	private class MyPageAdapter extends FragmentPagerAdapter {
		private List<Fragment> fragments;

		public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
			super(fm);
			this.fragments = fragments;
		}

		@Override
		public Fragment getItem(int position) {

			Fragment f = fragments.get(position);
			Bundle b = new Bundle();
			b.putString(EXTRA_MESSAGE, "Fragment #" + (position + 1));
			f.setArguments(b);
			
			return f;
		}

		@Override
		public int getCount() {
			return fragments.size();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * Removed class SectionsPagerAdapter.
	 * Since, we will be using MyPageAdapter instead,
	 * No use for it. 
	 */

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class Fragment_Main extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static Fragment_Main newInstance(int sectionNumber) {
			Fragment_Main fragment = new Fragment_Main();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber); // TODO
			fragment.setArguments(args);
			return fragment;
		}

		public Fragment_Main() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main2, container,
					false);
			TextView textView = (TextView) rootView
					.findViewById(R.id.section_label);
			textView.setText(Integer.toString(getArguments().getInt(
					ARG_SECTION_NUMBER)));
			return rootView;
		}
	}

}
